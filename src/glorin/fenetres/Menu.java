package glorin.fenetres;

import glorin.MainClass;
import glorin.outils.ConsoleAP;
import glorin.outils.LecteurNiveau;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Glorin
 */
public class Menu extends Fenetre {

    /**
     * Pour nommer les choix.
     */
    final static int OPTION_JOUER = 1;
    final static int OPTION_SCORES = 2;

    /**
     * Constructeur du menu
     *
     * @param fenetre
     */
    public Menu(MainClass fenetre) {
        super(fenetre);

        printHeader();

        System.out.println("1) Jouer");
        System.out.println("2) Tableau des scores");
        System.out.println("5) Quitter");
        ArrayList<Integer> acceptes = new ArrayList<>();
        acceptes.add(OPTION_JOUER);
        acceptes.add(OPTION_SCORES);
        acceptes.add(5);
        int choix = entree(acceptes);

        /**
         * Gère le choix
         */
        switch (choix) {
            case OPTION_JOUER:
                String nom = demanderUtilisateur();
                demanderNiveau(nom);
                break;
                
            case OPTION_SCORES:
                fenetre.changerFenetre(new Scores(fenetre));
                break;
                
            case 5:
                System.exit(1);
        }

    }

    /**
     * Demande le nom d'utilisateur
     */
    private String demanderUtilisateur() {
        // Demande le nom d'utilisateur        
        Scanner scan = new Scanner(System.in);

        String nom = "";

        do {
            System.out.print("Entrez votre nom : ");
            nom = scan.nextLine();
        } while (!nom.matches("[a-zA-Z]+"));

        return nom;
        
    }

    /**
     * Niveau
     */
    private void demanderNiveau(String nom) {
        LecteurNiveau ln = new LecteurNiveau();
        String[] selectionnerNiveau = ln.selectionnerNiveau();
        
        System.out.println("Choisissez un niveau :");
        ArrayList<Integer> choix = new ArrayList<>();
        int i = 1;
        for (String niveau : selectionnerNiveau) {
            System.out.println(i + ") " + niveau);
            choix.add(i);
            i++;
        }
/*<<<<<<< HEAD
        int n = entree(choix) - 1;

        fenetre.changerFenetre(new Jeu(fenetre, n, nom));

=======*/
        int n = entree(choix)-1;
        
        fenetre.console = new ConsoleAP();
        fenetre.changerFenetre(new JeuAP(fenetre, n));
        
    }

}
