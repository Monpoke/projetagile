package glorin.fenetres;

import glorin.MainClass;
import glorin.outils.ConsoleAP;
import glorin.outils.LecteurNiveau;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Glorin
 */
public class Scores extends Fenetre {

    /**
     * Pour nommer les choix.
     */
    final static int OPTION_JOUER = 1;

    /**
     * Constructeur du menu
     *
     * @param fenetre
     */
    public Scores(MainClass fenetre) {
        super(fenetre);
        headerInformations = "TABLEAU DES SCORES";
        printHeader();
        headerInformations = "";

        BufferedReader br = null;

        try {

            String sCurrentLine;
            System.out.println("PSEUDO\t\t\tSCORE\n");
            br = new BufferedReader(new FileReader("data/scores.txt"));

            while ((sCurrentLine = br.readLine()) != null) {
                String[] s = sCurrentLine.split(";");
                System.out.println(s[0] + "\t\t\t" + s[1]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println("\nPressez entréee pour retourner au menu...");
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
        fenetre.changerFenetre(new Menu(fenetre));
    }

}
