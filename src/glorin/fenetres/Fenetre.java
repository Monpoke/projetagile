package glorin.fenetres;

import glorin.Constante;
import glorin.Element;
import glorin.MainClass;
import glorin.Personnage;
import glorin.outils.OSValidator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Glorin
 */
public abstract class Fenetre {

    protected final MainClass fenetre;

    protected String headerInformations = "";

    public Fenetre(MainClass fenetre) {
        this.fenetre = fenetre;
    }

    /**
     * Permet de saisir un chiffre parmi une liste proposée
     *
     * @param choixAcceptes
     * @return
     */
    protected int entree(ArrayList<Integer> choixAcceptes) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String choix = "";
        boolean erreur = false;
        int choixN = -1;
        do {
            if (erreur == true) {
                System.out.print("Recommencez votre choix : ");
            } else {
                System.out.print("Faites un choix : ");
            }

            try {
                choix = br.readLine();
                choixN = Integer.parseInt(choix);
            } catch (IOException ex) {
                Logger.getLogger(Fenetre.class.getName()).log(Level.SEVERE, null, ex);
            } catch (java.lang.NumberFormatException nfe) {

            }
            erreur = true;
        } while (!choixAcceptes.contains(choixN));

        return choixN;
    }

    /**
     * Vérifies si un nombre est contenu dans un array.
     *
     * @param nb
     * @param choix
     * @return
     */
    protected boolean contains(int nb, int[] choix) {
        for (int i = 0; i < choix.length; i++) {
            if (nb == choix[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Envoie des retours à la ligne.
     */
    protected void effacerEcran() {
        if (OSValidator.isWindows()) {
            for (int i = 0; i < 100; i++) {
                System.out.println("");
            }
        } else {
            try {
                Runtime.getRuntime().exec("clear");
            } catch (final Exception e) {
                //  Handle any exceptions.
            }
        }
    }

    /**
     * Affiche le header du jeu
     */
    protected void printHeader() {
        effacerEcran();
        String h = "============================================================\n"
                + "  ___ _         _      \n"
                + " / __| |___ _ _(_)_ _  \n"
                + "| (_ | / _ \\ '_| | ' \\ \n"
                + " \\___|_\\___/_| |_|_||_|\n"
                + ((headerInformations.length() > 0) ? "\n============================================================\n" + headerInformations + "\n" : "")
                + "============================================================\n";

        System.out.println(h + "\n");
    }
    
    /**
     * Affiche l'ecran de victoire du jeu
     */
    protected void printWinScreen() {
        effacerEcran();
        String ws = "==============================================\n"
                + "  ___ _         _      \n"
                + " / __| |___ _ _(_)_ _  \n"
                + "| (_ | / _ \\ '_| | ' \\ \n"
                + " \\___|_\\___/_| |_|_||_|\n"
                + "\n"
                + "\n"
                //+ "Bien joué ! Vous venez de terminer ce niveau en"+ (Constante.viePersonnage - Personnage.getViePersonnage()) 
                + "\n"
                + "\n"
                + ((headerInformations.length() > 0) ? "\n==============\n" + headerInformations + "\n" : "")
                + "==============================================\n";
        
        System.out.println(ws + "\n");
//        Element.victoire = false;
    }
}
