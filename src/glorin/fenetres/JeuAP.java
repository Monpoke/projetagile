package glorin.fenetres;

import glorin.Constante;
import glorin.Element;
import glorin.Fin;
import glorin.MainClass;
import glorin.Mort;
import glorin.Personnage;
import glorin.Position;
import glorin.Sol;

public class JeuAP extends glorin.fenetres.Jeu {

    public static char toucheDeplacement;
    private String g = "";
    private int i = 0;
    private int lastDirection;

    // constructeur
    public JeuAP(MainClass fenetre, int fichierIdx) {
        super(fenetre, fichierIdx);

    }

    private char display() {
        fenetre.console.clearScreen();
        fenetre.console.cursor(0, 0);
        g += fenetre.console.key;
        toucheDeplacement = fenetre.console.key;

        headerInformations = "GLORIN\n"
                + "Char: " + fenetre.console.key + " Vie: " + joueur.getViePersonnage() + " - Coords: " + joueur.getCoordPersonnage().getX() + ":" + joueur.getCoordPersonnage().getY();
        System.out.println(headerInformations);

        /**
         * calcul min max
         */
        int[] minMax = aAfficher(plateauOriginal, joueur.getCoordPersonnage());
        int decalageHeader = 3;
        for (int y = 0; y < plateauAffichage.length; y++) {
            for (int x = minMax[0]; x < minMax[1]; x++) {
                fenetre.console.cursor(decalageHeader + y, x);
                System.out.print(plateauAffichage[y][x]);
            }
        }

        if (fenetre.console.key == '7') {
            System.exit(1);
        }

        char current = fenetre.console.key;

        // reset key
        fenetre.console.key = '\0';

        return current;
    }

    protected void initGame() {
        if (!continuerJeu) {
            copierPlateauAffichage(plateauOriginal);
        } else {
            initTimer();
        }
        continuerJeu = true;
        /**
         * Affichage du plateau
         */
        while (continuerJeu) {
            try {
                toucheDeplacement = display();

                if (toucheDeplacement == '9') {
                    resetGame();
                }

//                new Audio("assets/sons/kick.wav", 1000, false);
                deplaceJoueur(joueur, toucheDeplacement, joueur.getCoordPersonnage().getX(), joueur.getCoordPersonnage().getY());

                Thread.sleep(100);
            } catch (InterruptedException ex) {
            }

        }
    }

    protected boolean effectueAction() {
        switch (fenetre.console.key) {
            case '1':
            case '3':
                return true;
        }
        return false;
    }

    /**
     * Gére les déplacements joueurs
     *
     * @param joueur
     * @param touche
     * @param x
     * @param y
     * @return
     */
    private boolean deplaceJoueur(Personnage joueur, char touche, int x, int y) {
        Position pos = joueur.getCoordPersonnage();

        if (touche == '3') {
            x = pos.getX() + 1;
            lastDirection = 1;
            jumping = false;
        } else if (touche == '1') {
            x = pos.getX() - 1;
            lastDirection = -1;
            jumping = false;
        } else if (touche == '5') {
            y = pos.getY() - 2;
            x = pos.getX() + lastDirection;
            jumping = true;
        }

        if (!jumping) {
            if (joueur.getCoordPersonnage().getY() + 1 < plateauOriginal.length - 1) {
                if (!(plateauOriginal[joueur.getCoordPersonnage().getY() + 1][joueur.getCoordPersonnage().getX()] instanceof Sol)) {
                    y = pos.getY() + 1;   // gestion gravite
                }
            } else {
                gererMort();
                return false;
            }
        }

        /**
         * Verifier l utilité du return false false
         */
        if (checkInPlateau(x, y) == false) {
            return false;
        } else if (plateauOriginal[joueur.getCoordPersonnage().getY()][joueur.getCoordPersonnage().getX()].equals(Constante.drapeau)) {
            return false;
        }

        /**
         * @TOOD: Aucune vérif pour le moment. donc à faire
         *
         */
        return checkRecopiePlateauAffichage(x, y, touche, pos);
    }

    /**
     * On vérifie une case
     *
     * @param x
     * @param y
     * @param tou
     * @return
     */
    private boolean checkCase(int x, int y, int tou) {

        Element caseSuivante = null;

        int yDecalage = 0, xDecalage = 0;

        do {
            if (y + yDecalage > plateauOriginal.length - 1) {
                break;
            }

            caseSuivante = plateauOriginal[y + yDecalage][x];

//        System.out.println("x:y " + x + ":" + y + " -> verif " + xDecalage + ":" + yDecalage + " -> " + caseSuivante.display());
            yDecalage++;
        } while (yDecalage < 3 && caseSuivante instanceof Sol != true && caseSuivante instanceof Mort != true && caseSuivante instanceof Fin != true);

        /**
         * Case suivante
         */
        if (tou != 0) {
            if (caseSuivante.tue()) {
                gererMort();
                return false;
            } else if (yDecalage == 1 && caseSuivante.estBloquant()) {
                return false;
            } else if (caseSuivante.gagne()) {
                gererVictoire();
            }

        }

        return true;
    }

}
