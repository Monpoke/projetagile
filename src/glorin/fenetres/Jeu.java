package glorin.fenetres;

import glorin.Constante;
import glorin.Element;
import glorin.Ennemi;
import glorin.Fin;
import glorin.MainClass;
import glorin.Mort;
import glorin.Personnage;
import glorin.Position;
import glorin.Sol;
import glorin.outils.Audio;
import glorin.outils.LecteurNiveau;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Glorin
 */
public class Jeu extends Fenetre {

    protected Element[][] plateauOriginal;
    protected Element[][] plateauAffichage;

    private Ennemi ennemi = null;

    private int iEnnemi = 0;
    private boolean ennemiOnTop = false; // l'ennemi demarre du sol
    protected Personnage joueur = null;

    private Scanner sc;
    private char toucheDeplacement;
    private int i;

    protected boolean jumping = false;

    private final int LARGEUR_AFFICHEE_PLATEAU = 60;
    private final int LARGEUR_AVANT_JOUEUR = 30;
    protected boolean continuerJeu = true;
    private final LecteurNiveau ln;
    private final int fichierNiveau;
    private boolean fail = false;
    private long startTime;
    private String nom;

    /**
     * Default
     *
     * @param fenetre
     * @param fichierIdx
     */
    public Jeu(MainClass fenetre, int fichierIdx) {
        this(fenetre, fichierIdx, "Mario");
        fenetre.console.enableKeyTypedInConsole(true);
    }

    /**
     *
     * @param fenetre
     * @param fichierIdx
     * @param nom
     */
    public Jeu(MainClass fenetre, int fichierIdx, String nom) {
        super(fenetre);
        this.fichierNiveau = fichierIdx;
        this.nom = nom;

        ln = new LecteurNiveau();
        ln.setNomPerso(nom);
        loadPlateau();
        copierPlateauAffichage(plateauOriginal);
        initJoueur(ln);

        initGame();
    }

    /**
     * Affiche le plateau.
     */
    private void display() {
        headerInformations = "Vie: " + joueur.getViePersonnage() + " - Coords: " + joueur.getCoordPersonnage().getX() + ":" + joueur.getCoordPersonnage().getY();

        printHeader();

        /**
         * calcul min max
         */
        int[] minMax = aAfficher(plateauOriginal, joueur.getCoordPersonnage());

        for (int y = 0; y < plateauAffichage.length; y++) {
            for (int x = minMax[0]; x < minMax[1]; x++) {

                System.out.print(plateauAffichage[y][x]);
            }
            System.out.println("");
        }

    }

    /**
     * Retourne les coordoonnéees min max du plateau à afficher
     *
     * @param plateau
     * @param coords
     * @return
     */
    protected int[] aAfficher(Element[][] plateau, Position coords) {
        int min, max;

        min = coords.getX() - LARGEUR_AVANT_JOUEUR;

        // Min
        if ((coords.getX() - min) < LARGEUR_AVANT_JOUEUR || min < 0) {
            min = 0;
        }

        max = min + LARGEUR_AFFICHEE_PLATEAU;

        // Max
        if (max > plateau[0].length - 1) {
            max = plateau[0].length;
            min = max - LARGEUR_AFFICHEE_PLATEAU;
        }

        if(min<0) {
            min = 0 ;
        }
        
        return new int[]{min, max};
    }

    /**
     * Initialise un joueur
     *
     * @param ln
     */
    private void initJoueur(LecteurNiveau ln) {
        if (joueur == null) {
            joueur = new Personnage(nom);
        }
        Position pos = ln.getPositionJoueur();

        joueur.setCoordPersonnage(new Position(pos.getX(), pos.getY()));
        plateauAffichage[pos.getY()][pos.getX()] = joueur;
        // Déplace le joueur à l'origine
        deplaceJoueur(joueur, '2', joueur.getCoordPersonnage().getX(), joueur.getCoordPersonnage().getY());

    }
    
    private void initEnnemi(LecteurNiveau ln) {
        if(ennemi == null){
            ennemi = new Ennemi();
        }
        Position pos = ln.getPositionEnnemi();
    }

    /**
     * Recopie le tableau d'affichage.
     *
     * @param plateauOriginal
     */
    protected void copierPlateauAffichage(Element[][] plateauOriginal) {
        plateauAffichage = new Element[plateauOriginal.length][plateauOriginal[0].length];
        for (int i = 0; i < plateauOriginal.length; i++) {
            for (int j = 0; j < plateauOriginal[i].length; j++) {
                plateauAffichage[i][j] = plateauOriginal[i][j];
            }
        }
    }

    protected boolean checkInPlateau(int x, int y) {
        // evite sortie de plateau
        if (y < 0 || y > plateauAffichage.length || x < 0 || x > plateauAffichage[y].length - 1) {
            return false;
        } else {
            return true;
        }
    }

    protected boolean checkRecopiePlateauAffichage(int x, int y, char touche, Position pos) {
        if (checkCase(x, y, touche)) {
            // il faut inverser les cases joueurs
            plateauAffichage[pos.getY()][pos.getX()] = plateauOriginal[pos.getY()][pos.getX()];

            pos.setX(x);
            pos.setY(y);
            plateauAffichage[y][x] = joueur;
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param joueur
     * @param x
     * @param y
     * @return
     */
    private boolean deplaceJoueur(Personnage joueur, char touche, int x, int y) {
        Position pos = joueur.getCoordPersonnage();

        if (touche == '3') {
            x = pos.getX() + 1;
        } else if (touche == '1') {
            x = pos.getX() - 1;
        } else if (touche == 2){
            x = pos.getX() ;
        }

        if (!jumping && !(plateauOriginal[joueur.getCoordPersonnage().getY() + 1][joueur.getCoordPersonnage().getX()] instanceof Sol)) {
            y = pos.getY() + 1;   // gestion gravite
        }

        /**
         * Verifier l utilité du return false false
         */
        if (checkInPlateau(x, y) == false) {
            return false;
        } else if (plateauOriginal[joueur.getCoordPersonnage().getY()][joueur.getCoordPersonnage().getX()].equals(Constante.drapeau)) {
            return false;
        }

        /**
         * @TOOD: Aucune vérif pour le moment. donc à faire
         *
         */
        return checkRecopiePlateauAffichage(x, y, touche, pos);
    }

    /**
     * Vérifie une case
     *
     * @param x
     * @param y
     * @param tou
     * @return
     */
    private boolean checkCase(int x, int y, char tou) {

        Element caseSuivante = null;

        int yDecalage = 0, xDecalage = 0;

        do {
            if (y + yDecalage > plateauOriginal.length - 1) {
                break;
            }

            caseSuivante = plateauOriginal[y + yDecalage][x];

//            System.out.println("x:y " + x + ":" + y + " -> verif " + xDecalage + ":" + yDecalage + " -> " + caseSuivante.display());
            yDecalage++;
        } while (yDecalage < 3 && caseSuivante instanceof Sol != true && caseSuivante instanceof Mort != true && caseSuivante instanceof Fin != true);

        /**
         * Case suivante
         */
        if (tou != 0) {
            if (caseSuivante.tue()) {
                gererMort();
                return false;
            } 
            else if(yDecalage == 0 && caseSuivante instanceof Ennemi){
                gererMort();
                return false;
            }
            else if (yDecalage == 1 && caseSuivante.estBloquant()) {
                return false;
            } else if (caseSuivante.gagne()) {
                gererVictoire();
            }
        }
        return true;
    }

    
    private boolean deplacerEnnemi(Ennemi ennemi,char direction, int x, int y){
        Position pos = ennemi.getCoordEnnemi();
        
        if(direction == '8'){
            y = pos.getY()-1;
        }else if(direction == '2'){
            y = pos.getY()+1;
        }else if(direction == '6'){
            x = pos.getX()+1;
        }else if(direction == '4'){
            x = pos.getX()-1;
        }
        
        return checkRecopiePlateauAffichage(x, y, direction, pos);
    }
    /**
     * Gérer la mort.
     */
    protected void gererMort() {
        continuerJeu = false;

        System.out.println("Vous êtes mort.");

        if (joueur.getViePersonnage() > 1) {
            joueur.setViePersonnage(joueur.getViePersonnage() - 1);

            loadPlateau();
            copierPlateauAffichage(plateauOriginal);
            // recommence
            initJoueur(ln);

            initGame();
        } else {
            new Audio("assets/sons/gameover.wav", 0, false);
            try {
                Thread.sleep(3800);
            } catch (InterruptedException ex) {
            }

            resetGame();
        }

    }

    /**
     * Gérer victoire
     */
    protected void gererVictoire() {
        FileWriter fw = null;
        try {
            continuerJeu = false;
            long currentTime = System.currentTimeMillis();
            int diff = (int) ((currentTime - startTime) / 1000);
            int nb = (Constante.viePersonnage - joueur.getViePersonnage() + 1);
            int points = diff * 100 - (nb - 1) * 10;
            System.out.println("VOUS AVEZ GAGNE EN " + (diff) + " secondes et " + nb + " essai(s) !\nCela vous fait " + points + " points !");
            new Audio("assets/sons/win.wav", 0, false);
            try {
                Thread.sleep(5100);
            } catch (InterruptedException ex) {
            }
            String content = nom + ";" + points;
            File file = new File("data/scores.txt");
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(content + "\n");
            bw.close();
            resetGame();
        } catch (IOException ex) {
            Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Remet le menu
     */
    protected void resetGame() {
        fenetre.console.enableKeyTypedInConsole(false);
        System.out.println("\n\n");
        System.out.println("Retour vers le menu...");
        fenetre.changerFenetre(new Menu(fenetre));
    }

    private boolean sauteJoueur(Personnage joueur, char touche, int x, int y) {
        Position pos = joueur.getCoordPersonnage();

        if (touche == '1') {
            y = pos.getY() - 1;
            x = pos.getX() - 1;
        } else if (touche == '3') {
            y = pos.getY() - 1;
            x = pos.getX() + 1;
        }

        jumping = false;

        if (checkInPlateau(x, y) == false) {
            return false;
        } else if (plateauOriginal[joueur.getCoordPersonnage().getY()][joueur.getCoordPersonnage().getX()].equals(Constante.drapeau)) {
            return false;
        }

        return checkRecopiePlateauAffichage(x, y, touche, pos);
    }

    protected void initGame() {
        if (!continuerJeu) {
            copierPlateauAffichage(plateauOriginal);
        } else {
            initTimer();
        }
        continuerJeu = true;

        /**
         * Affichage du plateau
         */
        while (continuerJeu) {
            try {
                display();
                Thread.sleep(100);
                
                sc = new Scanner(System.in);
                
                if(ennemiOnTop){
                    iEnnemi--;
                }else{
                    iEnnemi++;
                }
                
                try {
                    // toucheDeplacement = sc.();

                    if (toucheDeplacement == 5 && plateauOriginal[joueur.getCoordPersonnage().getY() + 1][joueur.getCoordPersonnage().getX()] instanceof Sol) {
                        jumping = true;
                    }

                    if (toucheDeplacement == 9) {
                        continuerJeu = false;
                        fenetre.changerFenetre(new Menu(fenetre));
                    }
                } catch (Exception e) {
                }
                new Audio("assets/sons/kick.wav", 1000, false);
                if (jumping) {
                    //toucheDeplacement = sc.nextInt();
                    sauteJoueur(joueur, toucheDeplacement, joueur.getCoordPersonnage().getX(), joueur.getCoordPersonnage().getY()); // on simule en deux temps pour eviter les teleportations intramurales
                    sauteJoueur(joueur, toucheDeplacement, joueur.getCoordPersonnage().getX(), joueur.getCoordPersonnage().getY());
                } else {
                    deplaceJoueur(joueur, toucheDeplacement, joueur.getCoordPersonnage().getX(), joueur.getCoordPersonnage().getY());
                }
                
                if(iEnnemi == 4){
                    ennemiOnTop = true;
                }else if(iEnnemi == 0){
                    ennemiOnTop = false;
                }
                
                if(ennemiOnTop){
                    deplacerEnnemi(ennemi,'2', ennemi.getCoordEnnemi().getX(), ennemi.getCoordEnnemi().getY());
                }else{
                    deplacerEnnemi(ennemi,'8', ennemi.getCoordEnnemi().getX(), ennemi.getCoordEnnemi().getY());
                }
            } catch (InterruptedException ex) {
            }
        }
    }

    private void loadPlateau() {
        String[] fichiers = ln.selectionnerNiveau();
        String fichier = fichiers[this.fichierNiveau];
        plateauOriginal = ln.CSVIntoEnvironnement(fichier);
    }

    /**
     * Enregistre le temps de départ
     */
    protected void initTimer() {
        startTime = System.currentTimeMillis();
    }
}
