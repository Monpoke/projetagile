package glorin;

import glorin.fenetres.Fenetre;
import glorin.fenetres.Menu;
import glorin.outils.ConsoleAP;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

/**
 *
 * @author Glorin
 */
public class MainClass {

    private Fenetre fenetreActuelle;
    public ConsoleAP console;
    
    public static void main(String[] args) {
        
       new MainClass();
    }

    /**
     * Constructeur
     */
    public MainClass() {
        changerFenetre(new Menu(this));
    }

    public void changerFenetre(Fenetre fenetre) {
        fenetreActuelle = fenetre;
    }

    public ConsoleAP getConsole() {
        return console;
    }
    
    

}
