package glorin.outils;

import glorin.Element;
import glorin.Mort;
import glorin.Sol;
import glorin.Vide;
import java.util.Random;


public class GenerateurEnvironnement {
	
	static Element[][] tab = new Element[13][78];
	
        //78 de largeur
        
        
        
        static Element[][] generer(){
            tabVide();
            tabPlat();
            tabRandom();
            return tab;
        } 
        
        
        
        static void tabVide(){
            
            for(int i=0; i<13; i++){
                   for(int j=0; j<78; j++){                    
                       tab[i][j] = new Vide();
                   }
            }         
        }
        
        static void tabPlat(){
            
            for(int i=0; i<78; i++)
                tab[7][i] = new Sol();
            
            for(int f=8;f<13;f++){
                for(int j=0; j<78; j++){
                    tab[f][j] = new Mort();
                }
            }            
        }
        
        static void tabRandom(){
            
            boolean pose = true;
            int cpt = 0;
            Random r = new Random();
            //On ne met pas plus d'un vide toutes les 7 cases
            
            for(int i=0; i<78; i++){
                
                if(!pose){
                    // ajouter le random
                    if(r.nextInt(2)>0){
                        tab[7][i] = new Vide();
                        pose=true;
                        cpt = i;
                    }
                }         
                
                if(pose && i>2 && cpt<(i-7)){
                    pose=false;
                }                  
                               
                                
            }
            
        }
        
        public static void main(String[] args){
            
            Element[][] tabi = generer();
            System.out.println("tabi");
        }

}
