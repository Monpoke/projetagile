package glorin.outils;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class ConsoleAP {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_CURSOR_UP = "\u001B[1A";
    public static final String ANSI_CURSOR_DOWN = "\u001B[1B";
    public static final String ANSI_CURSOR_FORWARD = "\u001B[1C";
    public static final String ANSI_CURSOR_BACK = "\u001B[1D";
    public static final String ANSI_CURSOR_NEXT_LINE = "\u001B[1E";
    public static final String ANSI_CURSOR_PREV_LINE = "\u001B[1F";
    public static final String ANSI_CURSOR_MOVE_TO = "\u001B[3;3H";
    public static final String ANSI_CURSOR_SAVE_POS = "\u001B[s";
    public static final String ANSI_CURSOR_RESTORE_POS = "\u001B[u";
    public static final String ANSI_CURSOR_SHOW = "\u001B[?25h";
    public static final String ANSI_CURSOR_HIDE = "\u001B[?25l";

    public static final char ANSI_UP = 17;
    public static final char ANSI_DOWN = 18;
    public static final char ANSI_LEFT = 19;
    public static final char ANSI_RIGHT = 20;

    public static final String ANSI_CLEAR_SCREEN_AFTER = "\u001B[0J";
    public static final String ANSI_CLEAR_SCREEN_BEFORE = "\u001B[1J";
    public static final String ANSI_CLEAR_SCREEN_ALL = "\u001B[2J";
    public static final String ANSI_CLEAR_LINE_AFTER = "\u001B[0K";
    public static final String ANSI_CLEAR_LINE_BEFORE = "\u001B[1K";
    public static final String ANSI_CLEAR_LINE_ALL = "\u001B[2K";

    public static final String ANSI_SCROLL_UP = "\u001B[1S";
    public static final String ANSI_SCROLL_DOWN = "\u001B[1T";
    public static final String ANSI_BOLD = "\u001B[1m";
    public static final String ANSI_FAINT = "\u001B[2m";
    public static final String ANSI_ITALIC = "\u001B[3m";
    public static final String ANSI_UNDERLINE = "\u001B[4m";
    public static final String ANSI_BLINK_SLOW = "\u001B[5m";
    public static final String ANSI_BLINK_FAST = "\u001B[6m";
    public static final String ANSI_NEGATIVE = "\u001B[7m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_TEXT_DEFAULT_COLOR = "\u001B[39m";
    public static final String ANSI_BLACK_BG = "\u001B[40m";
    public static final String ANSI_RED_BG = "\u001B[41m";
    public static final String ANSI_GREEN_BG = "\u001B[42m";
    public static final String ANSI_YELLOW_BG = "\u001B[43m";
    public static final String ANSI_BLUE_BG = "\u001B[44m";
    public static final String ANSI_PURPLE_BG = "\u001B[45m";
    public static final String ANSI_CYAN_BG = "\u001B[46m";
    public static final String ANSI_WHITE_BG = "\u001B[47m";
    public static final String ANSI_BG_DEFAULT_COLOR = "\u001B[49m";

    private boolean listeningConsole = false;
    private Thread keyboardListener;
    public static char key;

    public ConsoleAP() {
        enableKeyTypedInConsole(true);
    }

    void keyTypedInConsole(char c) {
        key = c;
    }

    public void enableKeyTypedInConsole(boolean on) {
        if(OSValidator.isWindows()){
            return;
        }
        
        if (!listeningConsole && on) {
            listeningConsole = true;
            keyboardListener = new Thread() {
                public void run() {
                    try {
                        String[] cmd = {"/bin/sh", "-c", "stty raw</dev/tty"};
                        Runtime.getRuntime().exec(cmd).waitFor();
                        Console console = System.console();
                        Reader reader = console.reader();
                        while (listeningConsole) {
                            int c = reader.read();
                            if (c == 27) { // ESC
                                c = reader.read();
                                if (c == 91) { // [
                                    c = reader.read(); // directional arrow !
                                    switch (c) { // surcharging DC1-4 ...
                                        case 65:
                                            keyTypedInConsole(ANSI_UP);
                                            break;
                                        case 66:
                                            keyTypedInConsole(ANSI_DOWN);
                                            break;
                                        case 67:
                                            keyTypedInConsole(ANSI_RIGHT);
                                            break;
                                        case 68:
                                            keyTypedInConsole(ANSI_LEFT);
                                            break;
                                    }
                                } else {
                                    // System.err.println("Unknown ANSIcode : "+c);
                                }
                            } else {
                                keyTypedInConsole((char) c);
                            }
                            Thread.sleep(100);
                        }
                        reader.close();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
            };
            keyboardListener.start();
        } else {
            listeningConsole = false;
            try {
                String[] cmd = new String[]{"/bin/sh", "-c",
                    "stty sane</dev/tty"};
                Runtime.getRuntime().exec(cmd).waitFor();
            } catch (IOException io) {
                io.printStackTrace();
            } catch (InterruptedException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    class KeyboardThread implements Runnable {

        public String line = null;
        public final long timeout;

        public KeyboardThread(long timeout) {
            this.timeout = timeout;
        }

        @Override
        public void run() {
            final long start = System.currentTimeMillis();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    System.in));
            try {
                // to flush previous inputs !
                while (br.ready()) {
                    br.readLine();
                }
                while (System.currentTimeMillis() - start < timeout
                        && line == null) {
                    if (br.ready()) {
                        line = br.readLine();
                    }
                    Thread.sleep(250);
                }
            } catch (IOException ioe) {
                throw new RuntimeException(
                        "[iJava] IO error whilewaiting input with timeout !\n"
                        + ioe.getMessage());
            } catch (InterruptedException ex) {
                throw new RuntimeException(
                        "[iJava] Interruption errorwhile waiting input with timeout !\n"
                        + ex.getMessage());
            }
        }
    }

    public void reset() {
        text("black");
        background("white");
        print(ANSI_RESET);
    }

    public void show() {
        print(ANSI_CURSOR_SHOW);
    }

    public void hide() {
        print(ANSI_CURSOR_HIDE);
    }

    public void up(int n) {
        print(ANSI_CURSOR_UP.replace("[1", "[" + n));
    }

    public void down(int n) {
        print(ANSI_CURSOR_DOWN.replace("[1", "[" + n));
    }

    public void forward(int n) {
        print(ANSI_CURSOR_FORWARD.replace("[1", "[" + n));
    }

    public void backward(int n) {
        print(ANSI_CURSOR_BACK.replace("[1", "[" + n));
    }

    public void up() {
        up(1);
    }

    public void down() {
        down(1);
    }

    public void forward() {
        forward(1);
    }

    public void backward() {
        backward(1);
    }

    public void cusp() {
        print(ANSI_CURSOR_SAVE_POS);
    }

    public void curp() {
        print(ANSI_CURSOR_RESTORE_POS);
    }

    public void cursor(int line, int column) {
        print("\u001B[" + line + ";" + column + "H");
    }

    public void clearEOL() {
        print(ANSI_CLEAR_LINE_AFTER);
    }

    public void clearBOL() {
        print(ANSI_CLEAR_LINE_BEFORE);
    }

    public void clearLine() {
        print(ANSI_CLEAR_LINE_ALL);
    }

    public void clearScreen() {
        print(ANSI_CLEAR_SCREEN_ALL);
    }

    public void text(String color) {
        String c = "";
        if (color.equalsIgnoreCase("black")) {
            c = ANSI_BLACK;
        }
        if (color.equalsIgnoreCase("red")) {
            c = ANSI_RED;
        }
        if (color.equalsIgnoreCase("green")) {
            c = ANSI_GREEN;
        }
        if (color.equalsIgnoreCase("yellow")) {
            c = ANSI_YELLOW;
        }
        if (color.equalsIgnoreCase("blue")) {
            c = ANSI_BLUE;
        }
        if (color.equalsIgnoreCase("purple")) {
            c = ANSI_PURPLE;
        }
        if (color.equalsIgnoreCase("cyan")) {
            c = ANSI_CYAN;
        }
        if (color.equalsIgnoreCase("white")) {
            c = ANSI_WHITE;
        }
        System.out.print(c);
    }

    public static String[] ANSI_COLORS = new String[]{"black", "red",
        "green", "yellow", "blue", "purple", "cyan", "white"};

    public String randomANSIColor() {
        return ANSI_COLORS[(int) (Math.random() * ANSI_COLORS.length)];
    }

    public void background(String color) {
        String c = "";
        if (color.equalsIgnoreCase("black")) {
            c = ANSI_BLACK_BG;
        }
        if (color.equalsIgnoreCase("red")) {
            c = ANSI_RED_BG;
        }
        if (color.equalsIgnoreCase("green")) {
            c = ANSI_GREEN_BG;
        }
        if (color.equalsIgnoreCase("yellow")) {
            c = ANSI_YELLOW_BG;
        }
        if (color.equalsIgnoreCase("blue")) {
            c = ANSI_BLUE_BG;
        }
        if (color.equalsIgnoreCase("purple")) {
            c = ANSI_PURPLE_BG;
        }
        if (color.equalsIgnoreCase("cyan")) {
            c = ANSI_CYAN_BG;
        }
        if (color.equalsIgnoreCase("white")) {
            c = ANSI_WHITE_BG;
        }
        System.out.print(c);
    }

    protected void print(Object o) {
        System.out.print(o.toString());
    }

    private void _println(Object o) {
        // HACK: LF does not work in RAW terminal mode :(
        if (listeningConsole) {
            cusp();
            print(o);
            curp();
            down();
        } else {
            System.out.println(o.toString());
        }
    }

    public static void main(String[] args) {
        ConsoleAP partie = new ConsoleAP();
    }
}
