package glorin.outils;

import glorin.Element;
import glorin.Ennemi;
import glorin.Fin;
import glorin.Mort;
import glorin.Personnage;
import glorin.Position;
import glorin.Sol;
import glorin.Vide;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Classe qui permettra de lire un niveau à partir d'un fichier.
 *
 * @author Glorin
 */
public class LecteurNiveau {

    private Element[][] tabElements;

    BufferedReader br;
    PrintWriter pw;
    String ligne;
    String[] listeChar;
    private String nom;

    
    /**
     * 
     * @return 
     */
    public String[] selectionnerNiveau() {
        File folder = new File("niveaux/");
        File[] files = folder.listFiles();
        String[] returnFiles;

        ArrayList<String> returnsF = new ArrayList<>();
        
        int p = 0;
        for (File file : files) {
            if(file.getName().contains(".csv") && !file.getName().contains("TEST")){
                returnsF.add(file.getName().replace(".csv", ""));
            }
        }
        
        /**
         * Conversion array
         */
        returnFiles = new String[returnsF.size()];
        for (String returnsF1 : returnsF) {
            returnFiles[p++] = returnsF1;
        }
        
        return returnFiles;
    }

    public Element[][] CSVIntoEnvironnement(String fichier) {
        try {
            br = new BufferedReader(new FileReader("niveaux/"+fichier+".csv"));
            ligne = br.readLine();
            String flux = "";

            int largeurMax = 0, hauteurMax = 0;

            while (ligne != null) {
                flux += ligne + "\n";
                listeChar = ligne.split(";");

                largeurMax = (listeChar.length > largeurMax) ? listeChar.length : largeurMax;
                hauteurMax++;

                ligne = br.readLine();
            }

            tabElements = new Element[hauteurMax][largeurMax];

            traiterLignes(flux);

        } catch (FileNotFoundException fnfe) {
            System.err.print("File Not Found");
        } catch (IOException ioe) {
            System.err.println("IOE");
        }

        genererEnnemis();
        
        return tabElements;
    }

    /*
     }catch(FileNotFoundException fnfe){
     System.err.print("File Not Found");
     }catch(IOException ioe){
     System.err.print("IOE");
     }
     */
    // ----------------------------------
    private int TAILLE_PLATEAU = 100;

    private Position positionJoueur;
    
    private Position positionEnnemi;

    /**
     * Retourne un plateau.
     *
     * @return
     */
    public char[][] initPlateau() {
        int lines = 4, cols = TAILLE_PLATEAU;

        char[][] plateau = new char[lines][cols];

        for (int y = 0; y < lines; y++) {
            for (int x = 0; x < cols; x++) {
                plateau[y][x] = ' ';
            }
        }

        // sol
        int y2 = lines - 3;
        for (int x = 0; x < cols; x++) {
            if (x % 5 == 0) {
                plateau[y2][x] = '-';
            } else if (x % 2 == 0) {
                plateau[y2][x] = '=';
            } else {
                plateau[y2][x] = ' ';
            }
        }

        // ground
        for (int y = lines - 1; y < lines; y++) {
            for (int x = 0; x < cols; x++) {
                plateau[y][x] = '_';
            }
        }

        plateau[2][0] = 'X';
        positionJoueur = new Position(0, 2);
        
        positionEnnemi = new Position(0, 3);

        return plateau;
    }

    /**
     * Retourne la position du joueur.
     *
     * @return
     */
    public Position getPositionJoueur() {
        return positionJoueur;
    }
    
    public Position getPositionEnnemi(){
        return positionEnnemi;
    }

    /**
     * Traite les lignes
     *
     * @param flux
     */
    private void traiterLignes(String flux) {
        String[] lignes = flux.split("\n");
        for (int y = 0; y < lignes.length; y++) {
            String[] ch = lignes[y].split(";");

            for (int x = 0; x < ch.length; x++) {
                // System.out.print(ch[x]);

                Element elm = conversionType(ch[x], x, y);

                tabElements[y][x] = elm;

            }
            // System.out.println("");
        }
    }

    /**
     * Permet de retourner l'élément correspondant au caractère.
     * @param ch
     * @param x
     * @param y
     * @return 
     */
    private Element conversionType(String ch, int x, int y) {
        if (ch.equals("-")) {
            return new Sol();
        } else if (ch.equals("M")) {
            return new Mort();
        } else if (ch.equals("X")) {
            positionJoueur = new Position(x, y);
            return new Vide();
        } else if (ch.equals("I")) {
            return new Fin();
        }  else if (ch.equals("#")) {
            positionEnnemi = new Position(x,y);
            return new Vide();
        } else {
            return new Vide();
        }
    }

    /**
     * Cette fonction permet de générer 
     */
    private void genererEnnemis() {
              
    }

    public void setNomPerso(String nom) {
        this.nom = nom;
    }

}
