/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package glorin.outils;

import glorin.MainClass;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

/**
 *
 * @author Pierre
 */
public class Audio {

    public Audio(String file, int duration) {
        this(file, duration, true);
    }

    public Audio(String file, int duration, boolean loop) {

        try {
            AudioPlayer ap = AudioPlayer.player;
            AudioStream as = new AudioStream(new FileInputStream(file));

            if (loop) {
                AudioData AD = as.getData();
                ContinuousAudioDataStream CADC = new ContinuousAudioDataStream(AD);
                ap.start(CADC);
                
                Thread.sleep(duration);
                
            } else {
                ap.start(as);
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Fichier non trouvé!");
        } catch (IOException ex) {
            System.err.println("IO Ex");
        } catch (InterruptedException ex) {
            Logger.getLogger(Audio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
