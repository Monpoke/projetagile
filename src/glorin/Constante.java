package glorin;

/**
 *
 * @author Glorin
 */
public interface Constante {
    
    int viePersonnage = 3 ; 
    
    int coordDepartX = 1;
    
    int coordDepartY = 4;
    
    char floor = '-';
    
    char dead = ' ';
    
    char object = 'o';
    
    char air = ' ';
    
    char drapeau = 'I';
    
    char ennemy = '#';
    
    char character = 'O';
    
    int coordArriveeX = 1;
    
    int coordArriveeY = 20;
}
