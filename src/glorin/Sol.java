package glorin;

/**
 *
 * @author Glorin
 */
public class Sol extends Element {

    public char floor = Constante.floor;

    @Override
    public char display() {
        return '-';
    }

    @Override
    public boolean estBloquant() {
        return true;
    }

    @Override
    public String toString() {
        return ""+display();
    }
    
    

}
