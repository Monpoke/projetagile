package glorin;

/**
 *
 * @author Glorin
 */
public abstract class Element implements Constante {
    
    private boolean bloquant ;
    
    private Integer degats ;
    
    public boolean victoire ;

    public abstract char display();
    
    @Override
    public String toString() {
        return ""+display();
    }
    
    public boolean gagne(){
        return false;
    }
    
    public boolean tue() {
        return false;
    }
    
    public boolean estBloquant(){
        return false;
    }
}
