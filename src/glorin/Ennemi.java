package glorin;

/**
 *
 * @author Glorin
 */
public class Ennemi extends Mort {
    
    private Position coordEnnemi;
    
    @Override
    public char display(){
        return ennemy;
    }

    public void setCoordEnnemi(Position coordEnnemi) {
        this.coordEnnemi = coordEnnemi;
    }

    public Position getCoordEnnemi() {
        return coordEnnemi;
    }
}
