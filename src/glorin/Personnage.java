package glorin;

/**
 *
 * @author Glorin
 */
public class Personnage extends Element {
    
    private String nom ;
    
    private int viePersonnage ;
    
    private Position coordPersonnage ;
    
    public Personnage(String nom){
        this.nom = nom;     
        coordPersonnage = new Position(Constante.coordDepartX,Constante.coordDepartY);
        this.viePersonnage = Constante.viePersonnage;
    }
    
    public String getNom(){
        return this.nom;
    }
    
    private void setNom(String nom){
        this.nom = nom;
    }

    public Position getCoordPersonnage() {
        return coordPersonnage;
    }

    public void setCoordPersonnage(Position coordPersonnage) {
        this.coordPersonnage = coordPersonnage;
    }

    public void setViePersonnage(int viePersonnage) {
        this.viePersonnage = viePersonnage;
    }

    public int getViePersonnage() {
        return viePersonnage;
    }
    
    public char display() {
        return character;
    }
}
