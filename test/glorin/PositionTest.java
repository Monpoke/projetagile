/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class PositionTest {
    
    public PositionTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of getX method, of class Position.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Position instance = new Position(5, 5);
        int expResult = 5;
        int result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Position.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Position instance = new Position(1, 8);
        int expResult = 8;
        int result = instance.getY();
        assertEquals(expResult, result);
    }

    /**
     * Test of setX method, of class Position.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        int x = 8;
        Position instance = new Position(1,1);
        instance.setX(x);
    }

    /**
     * Test of setY method, of class Position.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        int y = 78;
        Position instance = new Position(8, 8);
        instance.setY(y);
    }

    /**
     * Test of toString method, of class Position.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Position instance = new Position(10, 10);
        String expResult = "[10:10]";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
