/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class PersonnageTest {
    
    private Personnage perso;
    
    public PersonnageTest() {
    }
    
    @Before
    public void setUp() {
        perso = new Personnage("Mario");
    }

    /**
     * Test of getCoordPersonnage method, of class Personnage.
     */
    @Test
    public void testGetCoordPersonnage() {
        System.out.println("getCoordPersonnage");
        Personnage instance = perso;
        perso.setCoordPersonnage(new  Position(2, 2));
        Position expResult = new Position(2, 2);
        Position result = instance.getCoordPersonnage();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCoordPersonnage method, of class Personnage.
     */
    @Test
    public void testSetCoordPersonnage() {
        System.out.println("setCoordPersonnage");
        Position coordPersonnage = new Position(4,4);
        Personnage instance = perso;
        instance.setCoordPersonnage(coordPersonnage);
        
    }

    /**
     * Test of setViePersonnage method, of class Personnage.
     */
    @Test
    public void testSetViePersonnage() {
        System.out.println("setViePersonnage");
        int viePersonnage = 100;
        Personnage instance = perso;
        instance.setViePersonnage(viePersonnage);
    }

    /**
     * Test of getViePersonnage method, of class Personnage.
     */
    @Test
    public void testGetViePersonnage() {
        System.out.println("getViePersonnage");
        Personnage instance = perso;
        instance.setViePersonnage(10);
        int expResult = 10;
        int result = instance.getViePersonnage();
        assertEquals(expResult, result);
    }

    /**
     * Test of display method, of class Personnage.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        Personnage instance = perso;
        char expResult = 'O';
        char result = instance.display();
        assertEquals(expResult, result);
    }

    /**
     * Test of display method, of class Personnage.
     */
    @Test
    public void testNom() {
        System.out.println("nom");
        Personnage instance = perso;
        String expResult = "Mario";
        String result = instance.getNom();
        assertEquals(expResult, result);
    }
    
}
