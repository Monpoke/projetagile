/*
 *  Glorin - Projet Agile
 */
package glorin.fenetres;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class FenetreTest {
    
    public FenetreTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of contains method, of class Fenetre.
     */
    @Test
    public void testContains() {
        System.out.println("contains");
        int nb = 5;
        int[] choix = new int[]{1,21,3,45,8,65,659};
        Fenetre instance = new FenetreImpl();
        boolean expResult = false;
        boolean result = instance.contains(nb, choix);
        assertEquals(expResult, result);
        
        assertTrue(instance.contains(659, choix));
    }



    public class FenetreImpl extends Fenetre {

        public FenetreImpl() {
            super(null);
        }
        
    }
    
}
