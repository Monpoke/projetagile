/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class VideTest {
    
    public VideTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of display method, of class Vide.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        Vide instance = new Vide();
        char expResult = ' ';
        char result = instance.display();
        assertEquals(expResult, result);
    }
    
}
