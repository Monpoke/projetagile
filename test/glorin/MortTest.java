/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class MortTest {
    
    public MortTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of display method, of class Mort.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        Mort instance = new Mort();
        char expResult = ' ';
        char result = instance.display();
        assertEquals(expResult, result);
    }

    /**
     * Test of tue method, of class Mort.
     */
    @Test
    public void testTue() {
        System.out.println("tue");
        Mort instance = new Mort();
        boolean expResult = true;
        boolean result = instance.tue();
        assertEquals(expResult, result);
    }
    
}
