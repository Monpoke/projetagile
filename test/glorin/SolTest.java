/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class SolTest {
    
    public SolTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of display method, of class Sol.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        Sol instance = new Sol();
        char expResult = '-';
        char result = instance.display();
        assertEquals(expResult, result);
    }

    /**
     * Test of estBloquant method, of class Sol.
     */
    @Test
    public void testEstBloquant() {
        System.out.println("estBloquant");
        Sol instance = new Sol();
        boolean expResult = true;
        boolean result = instance.estBloquant();
        assertEquals(expResult, result);
    }
    
}
