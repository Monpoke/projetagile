/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class FinTest {
    
    public FinTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of display method, of class Fin.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        Fin instance = new Fin();
        char expResult = 'I';
        char result = instance.display();
        assertEquals(expResult, result);
    }

    /**
     * Test of gagne method, of class Fin.
     */
    @Test
    public void testGagne() {
        System.out.println("gagne");
        Fin instance = new Fin();
        boolean expResult = true;
        boolean result = instance.gagne();
        assertEquals(expResult, result);
    }
    
}
