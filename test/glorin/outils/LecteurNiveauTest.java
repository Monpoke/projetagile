/*
 *  Glorin - Projet Agile
 */
package glorin.outils;

import glorin.Element;
import glorin.Position;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class LecteurNiveauTest {
    private LecteurNiveau ln;
    
    public LecteurNiveauTest() {
    }
    
    @Before
    public void setUp() {
        this.ln = new LecteurNiveau();
        this.ln.CSVIntoEnvironnement("TEST");
        System.out.println("setup in directory " + System.getProperty("user.dir"));
    }


    /**
     * Test of getPositionJoueur method, of class LecteurNiveau.
     */
    @Test
    public void testGetPositionJoueur() {
        System.out.println("getPositionJoueur");
        LecteurNiveau instance = ln;
        Position expResult = new Position(0,1);
        Position result = instance.getPositionJoueur();
        
        assertEquals(expResult, result);
    }
    
}
