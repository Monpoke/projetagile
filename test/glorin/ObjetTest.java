/*
 *  Glorin - Projet Agile
 */
package glorin;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pierre
 */
public class ObjetTest {
    
    public ObjetTest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of display method, of class Objet.
     */
    @Test
    public void testDisplay() {
        System.out.println("display");
        Objet instance = new Objet();
        char expResult = 'o';
        char result = instance.display();
        assertEquals(expResult, result);
    }
    
}
